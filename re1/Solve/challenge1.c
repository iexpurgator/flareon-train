#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *alphabet =
    "ZYXABCDEFGHIJKLMNOPQRSTUVWzyxabcdefghijklmnopqrstuvw0123456789+/";
const char *ascii =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
    "[\\]^`{|}~!\"#$%&'()*+,-./:;<=>?@";

char *encrypt(int Buffer, unsigned int len) {
    int key_2;
    int key_1;
    int key_0;
    int i;
    unsigned int index;
    char *res;
    int k;
    int j;
    unsigned int intptr;

    res = malloc(4 * ((len + 2) / 3) + 1);
    if (!res) return 0;
    intptr = 0;
    k = 0;
    while (intptr < len) {
        key_0 = *(unsigned char *)(intptr + Buffer);
        if (++intptr >= len) {
            key_1 = 0;
        } else {
            key_1 = *(unsigned char *)(intptr + Buffer);
            ++intptr;
        }
        if (intptr >= len) {
            key_2 = 0;
        } else {
            key_2 = *(unsigned char *)(intptr + Buffer);
            ++intptr;
        }
        index = key_2 + (key_0 << 16) + (key_1 << 8);
        res[k] = alphabet[(index >> 18) & 63];
        j = k + 1;
        res[j] = alphabet[(index >> 12) & 63];
        res[++j] = alphabet[(index >> 6) & 63];
        res[++j] = alphabet[key_2 & 63];
        k = j + 1;
    }
    for (i = 0; i < *(int *)&alphabet[4 * (len % 3) + 63]; ++i)
        res[4 * ((len + 2) / 3) - i - 1] = 61;
    res[4 * ((len + 2) / 3)] = 0;
    return res;
}

void brute(char c0, char c1, char c2, char c3) {
    int i, j;
    int k = 3;
    int n = strlen(ascii);
    int a[k + 1];
    for (i = 1; i <= k; i++) a[i] = 1;
    a[k] = 0;
    j = k;
    while (1) {
        if (a[j] == n) {
            j--;
            if (j == 0) break;
        } else {
            a[j]++;
            while (j < k) {
                j++;
                a[j] = 1;
            }
            //--------------------------------------
            char key[k];
            for (i = 1; i <= k; i++) key[i - 1] = ascii[a[i] - 1];
            int index = key[2] + (key[0] << 16) + (key[1] << 8);
            if (c0 == alphabet[(index >> 18) & 63] &&
                c1 == alphabet[(index >> 12) & 63] &&
                c2 == alphabet[(index >> 6) & 63] &&
                c3 == alphabet[key[2] & 63]) {
                printf("%c%c%c", key[0], key[1], key[2]);
                break;
            }
            //--------------------------------------
        }
    }
}

void decode(char *encoded) {
    char c0, c1, c2, c3;
    for (int i = 0; i < strlen(encoded) / 4; ++i) {
        c0 = encoded[i * 4];
        c1 = encoded[i * 4 + 1];
        c2 = encoded[i * 4 + 2];
        c3 = encoded[i * 4 + 3];
        brute(c0, c1, c2, c3);
        // printf("\n");
        // printf("%c%c%c%c\r\n", encoded[i * 4], encoded[i * 4 + 1],
        //        encoded[i * 4 + 2], encoded[i * 4 + 3]);
    }
}

int main() {
    // base64 custom alphabet
    char *encoded = "x2dtJEOmyjacxDemx2eczT5cVS9fVUGvWTuZWjuexjRqy24rV29q";
    decode(encoded);
    // brute(alphabet);
}