## <span style="color:#ff6347"> __*fidler*__ </span>
```python
last_value = 103079215104 // 10**8
encoded_flag = [1135, 1038, 1126, 1028, 1117, 1071, 1094, 1077, 1121, 1087,
                1110, 1092, 1072, 1095, 1090, 1027, 1127, 1040, 1137,
                1030, 1127, 1099, 1062, 1101, 1123, 1027, 1136, 1054]
decoded_flag = []

for i in range(len(encoded_flag)):
	c = encoded_flag[i]
	val = (c - ((i%2)*1 + (i%3)*2)) ^ last_value
	decoded_flag.append(val)
	last_value = c

print(''.join([chr(x) for x in decoded_flag]))
```

flag: idle_with_kitty@flare-on.com

## <span style="color:#ff6347"> __*challenge1.exe*__ </span>
Flag encoded on `Base64` custom alphabet: `ZYXABCDEFGHIJKLMNOPQRSTUVWzyxabcdefghijklmnopqrstuvw0123456789+/`

flag: sh00ting_phish_in_a_barrel@flare-on.com

## <span style="color:#ff6347"> __*login.html*__ </span>
Debug in browsers, can see input encode `ROT13`.

Decode ROT13: `PyvragFvqrYbtvafNerRnfl@syner-ba.pbz`. We got...

flag: ClientSideLoginsAreEasy@flare-on.com

## <span style="color:#ff6347"> __*MinesweeperChampionshipRegistration.jar*__ </span>
Open in `java decompiler` we got...

flag: GoldenTicket2018@flare-on.com

