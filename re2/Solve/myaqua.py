import hashlib
basexor = [
            0x96, 0x25, 0xa4, 0xa9, 0xa3, 0x96, 0x9a, 0x90, 0x9f, 0xaf, 0xe5, 0x38, 0xf9, 0x81, 0x9e, 0x16, # 0x1024F3A0
            0xf9, 0xcb, 0xe4, 0xa4, 0x87, 0x8f, 0x8f, 0xba, 0xd2, 0x9d, 0xa7, 0xd1, 0xfc, 0xa3, 0xa8 # 0x1024F390
        ]


def test():
    # unk_102D577C = b'DFWEyEW'
    # unk_102D5774 = b'newaui\x00\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xFE\x00'
    unk_102D577C = b'PXopvMDFWEyEWBGgsuhn'
    unk_102D5774 = b'SLdkvnewauiHwdwAZ'

    for i, v in enumerate(basexor):
        basexor[i] = (v ^ unk_102D577C[i% len(unk_102D577C)]) - unk_102D5774[i % 17]
        basexor[i] &= 0xff

    print(' '.join([hex(i)[2:] for i in basexor]))
    res = hashlib.md5(bytearray(basexor)).hexdigest()
    print(res)


def C_repeate(n, k): # combination with repetition
    r = []
    a = [1]* (k+1)
    a[k] = 0
    j = k
    while True:
        if a[j] == n:
            j -= 1
            if j == 0: break
        else:
            a[j] += 1
            while j < k:
                j += 1
                a[j] = 1
            r.append(a[1:])
    return r


def getMD5(a, b):
    if(len(b) <= 16): return None, None
    res = [0]*31
    for i, v in enumerate(basexor):
        res[i] = (v ^ a[i% len(a)]) - b[i % 17]
        res[i] &= 0xff
    rmd5 = hashlib.md5(bytearray(res)).hexdigest()
    return rmd5, res


arr_102D577C = [
    [3, b'DFWEyEW'],
    [4, b'PXopvM'],
    [13, b'BGgsuhn']
]

arr_102D5774 = [
    [10, b'HwdwAZ'],
    [11, b'SLdkv'],
    [7, b'newaui']
]

if __name__ == '__main__':
    # test()
    cr = C_repeate(3, 3)

    for v1 in cr:
        s1 = b''.join([arr_102D577C[i-1][1] for i in v1])
        for v2 in cr:
            s2 = b''.join([arr_102D5774[i-1][1] for i in v2])
            # print(s1, s2)
            rmd5, res  = getMD5(s1, s2)
            if rmd5 == '6c5215b12a10e936f8de1e42083ba184':
                print([arr_102D577C[i-1][0] for i in v1], [arr_102D5774[i-1][0] for i in v2])
                print(''.join([chr(i) for i in res]))
    # print(s1+s2)
