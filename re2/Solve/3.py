import socket

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect(('127.0.0.1', 2222))
    s.sendall((162).to_bytes(4, 'little'))
    data = s.recv(1024)

print(repr(data))