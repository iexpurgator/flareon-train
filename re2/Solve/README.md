# 3.exe
In IDA, Can see program using socket with local ip and port 2222

Using python test this connect:
``` python
import socket

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect(('127.0.0.1', 2222))
    s.sendall(b'Hello')
    data = s.recv(1024)

print(repr(data))
```

I got result: `b"Nope, that's not it."`

Check function of exe in IDA, we can see:
```C
__int16 __cdecl sub_4011E6(unsigned __int8 *a1, unsigned int a2)
{
  unsigned int v2; // edx
  unsigned __int16 v3; // cx
  unsigned __int16 v5; // di
  int v6; // esi
  unsigned __int16 i; // [esp+0h] [ebp-4h]

  v2 = a2;
  v3 = 255;
  for ( i = 255; v2; v3 = HIBYTE(v3) + (unsigned __int8)v3 )
  {
    v5 = i;
    v6 = v2;
    if ( v2 > 0x14 )
      v6 = 20;
    v2 -= v6;
    do
    {
      v5 += *a1;
      v3 += v5;
      ++a1;
      --v6;
    }
    while ( v6 );
    i = HIBYTE(v5) + (unsigned __int8)v5;
  }
  return (HIBYTE(i) + (unsigned __int8)i) | ((v3 << 8) + (v3 & 0xFF00));
}
```
and this function called by:
```C
int __usercall sub_401008@<eax>(int _EDI@<edi>, _DWORD *a2@<esi>)
{
  _BYTE *v2; // eax
  char v3; // dl
  bool v5; // cf
  unsigned int v6; // ett
  int v7; // edx
  int v9; // [esp-4h] [ebp-1Ch]
  char buf[4]; // [esp+10h] [ebp-8h] BYREF
  SOCKET s; // [esp+14h] [ebp-4h]
  int savedregs; // [esp+18h] [ebp+0h] BYREF

  s = sub_401121(buf);
  if ( !s )
    return 0;
  v2 = &loc_40107C;
  v3 = buf[0];
  do
  {
    *v2 = (v3 ^ *v2) + 34;
    ++v2;
  }
  while ( (int)v2 < (int)&loc_40107C + 121 );
  if ( (unsigned __int16)sub_4011E6(&loc_40107C, 121) == 64350 )
  {
    // loc_40107C
    /*
      some bytes can't decompiler to C
    */
    send(s, "Congratulations! But wait, where's my flag?", 43, 0);
  }
  else
  {
    send(s, "Nope, that's not it.", 20, 0);
  }
  closesocket(s);
  return WSACleanup();
}
```
I see this code changed value at `loc_40107C` and check condition `(unsigned __int16)sub_4011E6(&loc_40107C, 121) == 64350`

I write a program [3enc.C](./3enc.c) hope this can find input pass condition `(unsigned __int16)sub_4011E6(&loc_40107C, 121) == 64350`

I ran this program, I got `162` and I replace that to python script:

```python
import socket

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect(('127.0.0.1', 2222))
    s.sendall((162).to_bytes(4, 'little'))
    data = s.recv(1024)

print(repr(data))
```
Run this script, I got: `b"Congratulations! But wait, where's my flag?"`

I open `3.exe` in `ollydbg` and run python script, I got:

![Run in OllyDBG](./3_OllyDBG.png)

I can see `ESP` contain a string. I try debug it in IDA:

![Run in IDA](./3_IDA.png)

I got...

flag: `et_tu_brute_force@flare-on.com`

# reports.xls
Open `.xls` in excel:

![Open XLS](./report_xls_open.png)

Check `Developer\Visual Basic`

![Check Visual Basic](./report_xls_vba.png)

See that I know when open this xls and accept all permistion, function `folderol()` in [Sheet1.vba](./sheet1.vba) automatically runs

I convert [visual basic](./sheet1.vba) script to [python](./report.py).

When I run part of the code in file [python](./report.py):
```python
wabbit = canoodle(F_T_Text, 0, 168667, xertz)
wabbit = bytearray(wabbit)
f = open("stomp.mp3", "wb")
f.write(wabbit)
f.close()
```
I got a mp3 file:

![stomp.mp3 detail](./stomp_detail.png)

Some time osint i find some thing it related to 'P-Code'

So, I search `decompile p-code using python` on google. I got `Big5-sec/pcode2code` and `bontchev/pcodedmp`

I try `pcodedmp` but it not diff with visual basic on excel

I try with `pcode2code` and got [pcode](./pcode.vb). you can install it `python -m pip install pcode2code`:

In P-Code VBA function `folderol()` it change, I convert it to [python](./report.py):

When I run part of the code in file [python](./report.py):
```python
firkin = list('FLARE-ON')
buff = [ord(firkin[len(firkin) - i - 1]) for i in range(len(firkin))]

wabbit = canoodle(F_T_Text, 2, 285729, buff)
wabbit = bytearray(wabbit)
f = open("v.png", "wb")
f.write(wabbit)
f.close()
```

I got picture:

![Flag picture](./v.png)

flag: `thi5_cou1d_h4v3_b33n_b4d@flare-on.com@flare-on.com`