Unpack it: `upx -d myaquaticlife.exe -o myaqua.exe`
Chạy chương trình bấm chọn các hình ảnh và bấm vào dòng chữ ở giữa thì nhận được string `you chose.. poorly`.
Mở file trong IDA tìm string trên nhưng không có

Sau khi nhận được hint là kiểm tra trong `procmon` thì tìm thấy được chương trình sử dụng thư mục `C:\Users\<username>\AppData\Local\Temp\MMBPlayer\` để lưu dữ liệu kiểm tra trong thư mục thấy có file `.gif`, `.html`, `.dll`.

![](./myaquaticlife-procmon.png)

Kiểm tra file `fathom.dll` trong IDA, tìm `you chose.. poorly` thì thấy có xuất hiện. nhảy đến func có string này.

Đặt break point ở ngay đầu hàm `PluginFunc19` thì sau khi bấm vào dòng chữ ở giữa thì bị dừng ở break point, thử bấm vào các ảnh trước khi bấm dòng chữ thì thấy tại `off_7298577C` có xuất hiện string. Kiểm tra các function khác trong dll thì thấy có `GetFile` và `SetFile` đều có sử dụng địa chỉ trên. Đọc kĩ hai hàm thì thấy `SetFile` kiểm tra và gán string vào một số địa chỉ trước `off_7298577C` nên đặt break point vào đây để tiếp tục phân tích.

Theo số thứ tự các hình lần lượt từ 1 - 16 thu đc các string

` 1` -> `0x72985778` -> `MZZWP`

` 2` -> `0x72985770` -> `BAJkR`

` 3` -> `0x7298577C` -> `DFWEyEW`

` 4` -> `0x7298577C` -> `PXopvM`

` 5` -> `0x72985778` -> `LDNCVYU`

` 6` -> `0x72985778` -> `yXQsGB`

` 7` -> `0x72985774` -> `newaui`

` 8` -> `0x72985770` -> `QICMX`

` 9` -> `0x72985770` -> `rOPFG`

`10` -> `0x72985774` -> `HwdwAZ`

`11` -> `0x72985774` -> `SLdkv`

`12` -> `0x72985778` -> `LSZvYSFHW`

`13` -> `0x7298577C` -> `BGgsuhn`

`14` -> `0x72985778` -> `LSZvYSFHW`

`15` -> `0x72985778` -> `RTYXAc`

`16` -> `0x72985770` -> `GTXI`

trong hàm `PluginFunc19` chỉ thấy sử dụng `0x7298577C` và `0x72985774`. Hai chuỗi này được xor và trừ với một mảng cố định cho trước sau đó được đem đi tính hash MD5.

Thử chọn hình `3` và hình `7` thu được đoạn hash md5 `5d887f7153036a74e7375247a35af532`. Viết lại đoạn hash với đầu vào là hình `3` và hình`7` bằng python

```py
import hashlib
basexor = [
            0x96, 0x25, 0xa4, 0xa9, 0xa3, 0x96, 0x9a, 0x90, 0x9f, 0xaf, 0xe5, 0x38, 0xf9, 0x81, 0x9e, 0x16, # 0x1024F3A0
            0xf9, 0xcb, 0xe4, 0xa4, 0x87, 0x8f, 0x8f, 0xba, 0xd2, 0x9d, 0xa7, 0xd1, 0xfc, 0xa3, 0xa8 # 0x1024F390
        ]

unk_102D577C = b'DFWEyEW'
unk_102D5774 = b'newaui\x00\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xAB\xFE\x00' # dựa vào data do chương trình generate

for i, v in enumerate(basexor):
    basexor[i] = (v ^ unk_102D577C[i% len(unk_102D577C)]) - unk_102D5774[i % 17]
    basexor[i] &= 0xff

print(hashlib.md5(bytearray(basexor)).hexdigest())
```

Từ đây thấy được rằng sẽ phải chọn các hình để tạo thành hai chuỗi `unk_102D577C`, `unk_102D5774` phù hợp để thu được mã MD5 `6c5215b12a10e936f8de1e42083ba184` 

```py
import hashlib
basexor = [
            0x96, 0x25, 0xa4, 0xa9, 0xa3, 0x96, 0x9a, 0x90, 0x9f, 0xaf, 0xe5, 0x38, 0xf9, 0x81, 0x9e, 0x16, # 0x1024F3A0
            0xf9, 0xcb, 0xe4, 0xa4, 0x87, 0x8f, 0x8f, 0xba, 0xd2, 0x9d, 0xa7, 0xd1, 0xfc, 0xa3, 0xa8 # 0x1024F390
        ]

def C_repeate(n, k): # combination with repetition
    r = []
    a = [1]* (k+1)
    a[k] = 0
    j = k
    while True:
        if a[j] == n:
            j -= 1
            if j == 0: break
        else:
            a[j] += 1
            while j < k:
                j += 1
                a[j] = 1
            r.append(a[1:])
    return r


def getMD5(a, b):
    if(len(b) <= 16): return None, None
    res = [0]*31
    for i, v in enumerate(basexor):
        res[i] = (v ^ a[i% len(a)]) - b[i % 17]
        res[i] &= 0xff
    rmd5 = hashlib.md5(bytearray(res)).hexdigest()
    return rmd5, res


arr_102D577C = [
    [3, b'DFWEyEW'],
    [4, b'PXopvM'],
    [13, b'BGgsuhn']
]

arr_102D5774 = [
    [10, b'HwdwAZ'],
    [11, b'SLdkv'],
    [7, b'newaui']
]

if __name__ == '__main__':
    cr = C_repeate(3, 3)

    for v1 in cr:
        s1 = b''.join([arr_102D577C[i-1][1] for i in v1])
        for v2 in cr:
            s2 = b''.join([arr_102D5774[i-1][1] for i in v2])
            rmd5, res  = getMD5(s1, s2)
            if rmd5 == '6c5215b12a10e936f8de1e42083ba184':
                print([arr_102D577C[i-1][0] for i in v1], [arr_102D5774[i-1][0] for i in v2])
                print(''.join([chr(i) for i in res]))

```

![](./myaquaticlife-flag.png)

flag: `s1gn_my_gu357_b00k@flare-on.com`